package com.cognizant.retailmate.screen;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognizant.retailmate.screen.Adapter.RecyclerViewAdapter;
import com.cognizant.retailmate.screen.Model.ItemObject;

import java.util.ArrayList;
import java.util.List;




public class Category1 extends Fragment {

    private GridLayoutManager lLayout;

    private List<ItemObject> getAllItemList(){

        List<ItemObject> allItems = new ArrayList<ItemObject>();
        allItems.add(new ItemObject("specs1", R.drawable.s1));
        allItems.add(new ItemObject("spec2", R.drawable.s2));
        allItems.add(new ItemObject("Uni", R.drawable.s3));
        allItems.add(new ItemObject("Gy", R.drawable.s4));
        allItems.add(new ItemObject("Sn", R.drawable.s5));
        Log.e("##@@", String.valueOf(allItems.size()));


        return allItems;
    }


    public Category1() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getAllItemList();
        lLayout = new GridLayoutManager(getContext(),  2);


        List<ItemObject> rowListItem = getAllItemList();

        RecyclerViewAdapter rcAdapter = new RecyclerViewAdapter(getContext(),rowListItem);


        View rootView = inflater.inflate(R.layout.category1, container, false);

        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        rv.setHasFixedSize(true);


        rv.setAdapter(rcAdapter);


        rv.setLayoutManager(lLayout);

        return rootView;
    }


    }


