package com.cognizant.retailmate.screen;

import android.support.v4.app.FragmentActivity;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cognizant.retailmate.screen.Adapter.CustomPagerAdapter;
import com.cognizant.retailmate.screen.Adapter.RecyclerAdapter;
import com.cognizant.retailmate.screen.Adapter.ViewPagerAdapter;
import com.cognizant.retailmate.screen.Model.ProductModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends FragmentActivity {

    private ViewPager mViewPager;
    private CustomPagerAdapter mCustomPagerAdapter;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;

    ImageView home_i,bag_i,plus_more,chat_i,profile_i;
    View home_v,bag_v,chat_v,profile_v;
    LinearLayout bottom_nav;
    View bottom_view;
    LinearLayout home,bag,chat,profile;

    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;

    Animation slideUpAnimation, slideDownAnimation,rotate360;

    /**Images used in the Image Slider**/
    int[] mResources = {
            R.drawable.one,
            R.drawable.two,
            R.drawable.three,
            R.drawable.four
    };

    List<ProductModel> productList = new ArrayList<>();
    private View features_more;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        slideUpAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);

        slideDownAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);

        rotate360 = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate360);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.CollapsingToolbarLayout);
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);

        bottom_view=findViewById(R.id.bottom_view);
        features_more=findViewById(R.id.more_container);

        bottom_nav = (LinearLayout) findViewById(R.id.bottom_navigation);

        home=(LinearLayout)findViewById(R.id.home);
        bag=(LinearLayout)findViewById(R.id.bag);
        chat=(LinearLayout)findViewById(R.id.chat);
        profile=(LinearLayout)findViewById(R.id.profile);

        plus_more = (ImageView) findViewById(R.id.more_features);

        home_v = findViewById(R.id.home_view);
        home_i=(ImageView)findViewById(R.id.home_icon);

        bag_v = findViewById(R.id.bag_view);
        bag_i=(ImageView)findViewById(R.id.bag_icon);

        chat_v = findViewById(R.id.chat_view);
        chat_i=(ImageView)findViewById(R.id.chat_icon);

        profile_v = findViewById(R.id.profile_view);
        profile_i=(ImageView)findViewById(R.id.profile_icon);

        /**Initialize the Bottom Nav**/
        home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.green));
        home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.green));

        bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
        bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.black));

        chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
        chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.transparent));

        profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
        profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.black));


        //To check if appbar is open or closed
       /** Show or Hide Bottom Navigation Bar **/
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                Log.e("##$$",String.valueOf(collapsingToolbarLayout.getHeight()+ verticalOffset)+" ** "+String.valueOf(2* ViewCompat.getMinimumHeight(collapsingToolbarLayout)));
                if (collapsingToolbarLayout.getHeight() + verticalOffset <(2* ViewCompat.getMinimumHeight(collapsingToolbarLayout))) {
                    Log.e("##$$","Closed");
                    bottom_view.startAnimation(slideDownAnimation);
                    bottom_view.setVisibility(View.GONE);

                } else {
                    Log.e("##$$","Opened");
                    bottom_view.setVisibility(View.VISIBLE);
//                    bottom_view.startAnimation(slideUpAnimation);
                }
            }
        });

//        Log.e("@@##",String.valueOf(mResources.length));

        /**Top Image Sliding View**/
        mViewPager = (ViewPager) findViewById(R.id.pager);
        TabLayout tabLayout_dots = (TabLayout) findViewById(R.id.tabDots);
        tabLayout_dots.setupWithViewPager(mViewPager, true);

        mCustomPagerAdapter = new CustomPagerAdapter(this,mResources);
        mViewPager.setAdapter(mCustomPagerAdapter);

        /** To make the recycler view horizontal*/
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView = (RecyclerView) findViewById(R.id.products_container);
        recyclerView.setLayoutManager(layoutManager);

        /**Loading product data to be displayed on recycler view**/
        populateData();

        recyclerAdapter= new RecyclerAdapter(getApplicationContext(),productList);
        recyclerView.setAdapter(recyclerAdapter);


        /** Initializing the Tab Layout with View Pager**/
        tabLayout=(TabLayout) findViewById(R.id.tabLayout);
        viewPager=(ViewPager) findViewById(R.id.viewPager);

        viewPagerAdapter= new ViewPagerAdapter(getSupportFragmentManager());

        /**Adding Tabs into tab layout*/
        viewPagerAdapter.addFragments(new Category1(),"Catogory 1");
        viewPagerAdapter.addFragments(new Category2(),"Category 2");
        viewPagerAdapter.addFragments(new Category3(),"Category 3");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);


        /**Bottom Navigation Bar Functions**/
        /**Home**/
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.green));
                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.green));

                bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.black));

                chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.transparent));

                profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.black));
            }
        });

        /**Bag**/
        bag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.black));

                chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.transparent));

                profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.black));

                bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.green));
                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.green));
            }
        });

        /**Chat**/
        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.black));

                bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.black));

                profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.black));

                chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.green));
                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.transparent));
            }
        });

        /**Profile**/
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_v.setBackgroundColor(getResources().getColor(R.color.transparent));
                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.black));

                bag_v.setBackgroundColor(getResources().getColor(R.color.transparent));
                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.black));

                chat_v.setBackgroundColor(getResources().getColor(R.color.transparent));
                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.transparent));

                profile_v.setBackgroundColor(getResources().getColor(R.color.green));
                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.green));
            }
        });

        /**More Features**/
        plus_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(features_more.getVisibility()== View.GONE){
                    plus_more.startAnimation(rotate360);
                    features_more.bringToFront();
                    features_more.setVisibility(View.VISIBLE);
                    features_more.startAnimation(slideUpAnimation);
                }
                else
                {   plus_more.startAnimation(rotate360);
                    features_more.startAnimation(slideDownAnimation);
                    features_more.setVisibility(View.GONE);
                }

//                Features features = new Features();
//                if (findViewById(R.id.more_features) != null) {
//                    getSupportFragmentManager().beginTransaction()
//                            .add(R.id.frag_container, features).commit();
//                }
//                else
//                {
//                    getSupportFragmentManager().beginTransaction().remove(features).commit();
//                    getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentById(R.id.frag_container)).commit();
//                }
            }
        });

    }

    private void populateData() {

        ProductModel model= new ProductModel();
        model.setName("Camera");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 124.0);
        productList.add(model);

        model=new ProductModel();
        model.setName("Tripod");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 12.0);
        productList.add(model);

        model= new ProductModel();
        model.setName("Camera");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 124.0);
        productList.add(model);

        model=new ProductModel();
        model.setName("Tripod");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 12.0);
        productList.add(model);

    }
}
