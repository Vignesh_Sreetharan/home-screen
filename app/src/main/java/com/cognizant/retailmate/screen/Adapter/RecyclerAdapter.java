package com.cognizant.retailmate.screen.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cognizant.retailmate.screen.Model.ProductModel;
import com.cognizant.retailmate.screen.R;

import java.util.List;

/**
 * Created by 599654 on 2/17/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    List<ProductModel> productList;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name,category,price;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            category = (TextView) view.findViewById(R.id.category);
            price = (TextView) view.findViewById(R.id.price);

        }
    }

    public RecyclerAdapter(Context context, List<ProductModel> productList){
        this.productList=productList;
    }

    @Override
    public RecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_details_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.MyViewHolder holder, int position) {

        ProductModel model = productList.get(position);

        holder.name.setText(model.getName());
        holder.category.setText(model.getCategory());
        holder.price.setText("$"+ String.valueOf(model.getPrice()));

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
