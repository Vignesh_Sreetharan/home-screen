package com.cognizant.retailmate.screen.Adapter;

/**
 * Created by 599584 on 2/17/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.retailmate.screen.Model.ItemObject;
import com.cognizant.retailmate.screen.R;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    private List<ItemObject> itemList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView productname;
        public ImageView Photo;

        public MyViewHolder(View view) {
            super(view);
            productname = (TextView)itemView.findViewById(R.id.productname);
            Photo = (ImageView)itemView.findViewById(R.id.photo);
        }
    }

    public RecyclerViewAdapter(Context context, List<ItemObject> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public RecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardviewlist, parent, false);

        return new RecyclerViewAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.productname.setText(itemList.get(position).getName());
        holder.Photo.setImageResource(itemList.get(position).getPhoto());
    }


    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}
