package com.cognizant.retailmate.screen.Model;

/**
 * Created by 599654 on 2/17/2017.
 */
//model class for recommendations recycler view.

public class ProductModel {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    private String name;
    private String category;
    private float price;
}
